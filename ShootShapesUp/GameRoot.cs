﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Content;
using System;
using System.Linq;

namespace ShootShapesUp
{
    public class GameRoot : Game
    {
        // some helpful static properties
        public static GameRoot Instance { get; private set; }
        public static Viewport Viewport { get { return Instance.GraphicsDevice.Viewport; } }
        public static Vector2 ScreenSize { get { return new Vector2(Viewport.Width, Viewport.Height); } }
        public static GameTime GameTime { get; private set; }

        public static Texture2D Player { get; private set; }

        public static Texture2D Seeker { get; private set; }
        public static Texture2D Bullet { get; private set; }

        public static Texture2D Boss { get; private set; }
        public static Texture2D Pill { get; private set; }
        public static Texture2D DoubleShot { get; private set; }
        public static Texture2D Heart { get; private set; }
        public static Texture2D Pointer { get; private set; }
        

        public static SpriteFont Font { get; private set; }

        public static Song Music { get; private set; }

        private static readonly Random rand = new Random();

        private static SoundEffect[] explosions;
        // return a random explosion sound
        public static SoundEffect Explosion { get { return explosions[rand.Next(explosions.Length)]; } }

        private static SoundEffect[] shots;
        public static SoundEffect Shot { get { return shots[rand.Next(shots.Length)]; } }

        private static SoundEffect[] spawns;
        public static SoundEffect Spawn { get { return spawns[rand.Next(spawns.Length)]; } }

        public static int NumberOfRounds = 3;

        public static int TotalScore { get { return score; } }

        static int score = 0;
        static int scoreBuffer = 0;
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        private Texture2D background;
        public static int enemiesPerRound = 30;

        static public void Score(int scorebufferInsert)
        {
            scoreBuffer += scorebufferInsert;
        }
        static public void AddScore()
        {
            if (scoreBuffer >= 1)
            {
                score += 5;
                scoreBuffer -= 5;
            }
        }
        static public void ScoreReset()
        {
            score = 0;
            scoreBuffer = 0;
        }
        public GameRoot()
        {
            Instance = this;
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = @"Content";

            this.IsMouseVisible = true;
            graphics.PreferredBackBufferWidth = 600;
            graphics.PreferredBackBufferHeight = 800;
        }

        protected override void Initialize()
        {
            base.Initialize();

            EntityManager.Add(PlayerShip.Instance);

            MediaPlayer.IsRepeating = true;
            MediaPlayer.Play(GameRoot.Music);
            

        }

        protected override void LoadContent()
        {

            spriteBatch = new SpriteBatch(GraphicsDevice);
            Player = Content.Load<Texture2D>("Art/Player");
            Seeker = Content.Load<Texture2D>("Art/Seeker");
            Bullet = Content.Load<Texture2D>("Art/Bullet");
            Pointer = Content.Load<Texture2D>("Art/Pointer");
            background = Content.Load<Texture2D>("Art/stars");
            Pill = Content.Load<Texture2D>("Art/Pill");
            Boss = Content.Load<Texture2D>("Art/Wanderer");
            DoubleShot = Content.Load<Texture2D>("Art/DoubleShot");
            Heart = Content.Load<Texture2D>("Art/Heart");
            Font = Content.Load<SpriteFont>("Font");

            Music = Content.Load<Song>("Sound/Music");


            // These linq expressions are just a fancy way loading all sounds of each category into an array.
            explosions = Enumerable.Range(1, 8).Select(x => Content.Load<SoundEffect>("Sound/explosion-0" + x)).ToArray();
            shots = Enumerable.Range(1, 4).Select(x => Content.Load<SoundEffect>("Sound/shoot-0" + x)).ToArray();
            spawns = Enumerable.Range(1, 8).Select(x => Content.Load<SoundEffect>("Sound/spawn-0" + x)).ToArray();
            
        }
        public static int MaxEnemiesPerRound{get { return enemiesPerRound; }private set { }}
        public static int maxFramesBetweenRounds = 120;
        public static int framesBetweenRound = 120;
        public static int howToPlayDisplayFrames = 240;
        public static bool betweenRounds = true;
        protected override void Update(GameTime gameTime)
        {
            // Allows the game to exit
            if (Input.WasButtonPressed(Buttons.Back) || Input.WasKeyPressed(Keys.Escape))
                this.Exit();
            if ((Input.WasButtonPressed(Buttons.A) || Input.WasKeyPressed(Keys.Space)) && GameOverScreen.GetGameStatus == true)
            {
                ScoreReset();
                NewLevelDisplay.Reset();
                GameOverScreen.GetGameStatus = false;
            }

            Input.Update();
            
            AddScore();
            if (GameOverScreen.GetGameStatus == false)
            {
                RunGame(gameTime);
            }

            if (howToPlayDisplayFrames > 0)
                howToPlayDisplayFrames--;
            base.Update(gameTime);
        }

        public static void RunGame(GameTime gameTime)
        {
            if (NewLevelDisplay.GetLevelNumber % NumberOfRounds == 0)
                enemiesPerRound = 1;
            else
            {
                enemiesPerRound = 10 * NewLevelDisplay.GetLevelNumber;
            }
            EntityManager.Update();
            GameTime = gameTime;
            if (EnemySpawner.GetEnemiesSpawned < enemiesPerRound && !betweenRounds && framesBetweenRound == 0)
            {
                EnemySpawner.Update();
            }
            else if (EnemySpawner.GetEnemiesSpawned >= enemiesPerRound && !betweenRounds && framesBetweenRound == 0 && EntityManager.GetEnemiesSpawned == 0 && !PlayerShip.Instance.IsDead)
            {
                betweenRounds = true;
                NewLevelDisplay.NextLevel();
                framesBetweenRound = maxFramesBetweenRounds;
                EnemySpawner.GetEnemiesSpawned = 0;
            }

            if (framesBetweenRound > 0)
                framesBetweenRound--;
            if (framesBetweenRound == 0)
                betweenRounds = false;
        }

        public static void HowToPlay(SpriteBatch spriteBatch)
        {
            spriteBatch.Begin();
            spriteBatch.DrawString(Font, "WASD to Move, Q to Screen Wipe", new Vector2(ScreenSize.X / 3, ScreenSize.Y - 30), Color.White);
            spriteBatch.End();
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);
            NewLevelDisplay.Draw(spriteBatch);
            // Draw entities. Sort by texture for better batching.
            spriteBatch.Begin(SpriteSortMode.Texture, BlendState.Additive);

            EntityManager.Draw(spriteBatch);
            spriteBatch.End();

            // Draw user interface
            spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.Additive);
            
            spriteBatch.Draw(background,Vector2.Zero,Color.White);
            spriteBatch.DrawString(Font, score.ToString(), new Vector2(10,20), Color.White);
            spriteBatch.DrawString(Font, "Enemies: "+ enemiesPerRound, new Vector2(ScreenSize.X-200, 20), Color.White);
            spriteBatch.DrawString(Font, "Lives "+PlayerShip.LivesRemaining().ToString(), new Vector2(ScreenSize.X-100,ScreenSize.Y-30), Color.White);
            spriteBatch.DrawString(Font, "Screen Wipes: " + PlayerShip.BossPowerUps.ToString(), new Vector2(20, ScreenSize.Y - 30), Color.White);
            spriteBatch.End();
            if (GameOverScreen.GetGameStatus == true)
                GameOverScreen.Draw(spriteBatch);

            if (howToPlayDisplayFrames > 0)
                HowToPlay(spriteBatch);

            base.Draw(gameTime);
        }

        private void DrawRightAlignedString(string text, float y)
        {
            var textWidth = GameRoot.Font.MeasureString(text).X;
            spriteBatch.DrawString(GameRoot.Font, text, new Vector2(ScreenSize.X - textWidth - 5, y), Color.White);
        }
    }
}
