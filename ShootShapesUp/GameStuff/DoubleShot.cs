﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShootShapesUp
{
    class DoubleShot : Entity
    {
        public DoubleShot(Vector2 position, Vector2 velocity)
        {
            image = GameRoot.DoubleShot;
            Position = position;
            Velocity = velocity;
            Radius = 15;
        }

        public override void Update()
        {
            this.color = Color.OrangeRed;
            Position += Velocity;
            
            // delete bullets that go off-screen
            if (!GameRoot.Viewport.Bounds.Contains(Position.ToPoint()))
                IsExpired = true;
            if (Position.Y >= GameRoot.ScreenSize.Y)
            {
                IsExpired = true;
            }
        }

    }
}
