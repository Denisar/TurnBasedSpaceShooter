﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShootShapesUp
{
    class Enemy : Entity
    {
        #region Properties
        public static Random rand = new Random();
        private List<IEnumerator<int>> behaviours = new List<IEnumerator<int>>();
        List<EnemyBullet> bullets = new List<EnemyBullet>();
        private int timeUntilStart = 60;
        public bool IsActive { get { return timeUntilStart <= 0; } }
        public int PointValue { get; private set; }
        public Color pickedColor;
        public int health;
        public int Score;
        public bool isAbleToFire;
        int cooldownFrames;
        int cooldownRemaining = 0;
        String enemyType;
        double ChanceOfUpgrade = 0.2;
        double ChanceOfExtraLife = 0.9;

        #endregion
        #region Methods 
        public Enemy(Texture2D image, Vector2 position)
        {
            this.image = image;
            EnemyTypePicker();
            DifficultyMultiplier();
            Position = position;
            Radius = image.Width / 2f;
            color = Color.Transparent;
            PointValue = 1;
            
            
        }
     
        public void EnemyTypePicker()
        {
            if (NewLevelDisplay.GetLevelNumber%GameRoot.NumberOfRounds != 0)
            {
                switch (rand.Next(1, 4))
                {
                    case 1:
                        pickedColor = Color.Orange;
                        health = 10;
                        Score = 50;
                        isAbleToFire = false;
                        enemyType = "Yellow";
                        break;
                    case 2:
                        pickedColor = Color.White;
                        health = 20;
                        Score = 100;
                        isAbleToFire = true;
                        cooldownFrames = 80;
                        enemyType = "White";
                        break;
                    case 3:
                        pickedColor = Color.Green;
                        health = 30;
                        Score = 150;
                        isAbleToFire = true;
                        cooldownFrames = 60;
                        enemyType = "Green";
                        break;
                }
            }else
            {
                pickedColor = Color.Red;
                health = 200;
                Score = 1000;
                isAbleToFire = true;
                cooldownFrames = 10;
                enemyType = "Boss";
                this.image = GameRoot.Boss;
            }
        }
        public void DifficultyMultiplier()
        {
            health *= NewLevelDisplay.GetLevelNumber;
        }

        public static Enemy CreateSeeker(Vector2 position)
        {
            var enemy = new Enemy(GameRoot.Seeker, position);
            enemy.AddBehaviour(enemy.Movemnet());
            enemy.PointValue = 2;

            return enemy;
        }
        
        public override void Update()
        {
            

            if (timeUntilStart <= 0)
                ApplyBehaviours();
            else
            {
                timeUntilStart--;
                color = pickedColor * (1 - timeUntilStart / 60f);
            }
            if (isAbleToFire)
            {
                if (cooldownRemaining <= 0)
                {
                    cooldownRemaining = cooldownFrames;
                    float aimAngle = (PlayerShip.Instance.Position - Position).ToAngle();
                    Quaternion aimQuat = Quaternion.CreateFromYawPitchRoll(0, 0, aimAngle);

                    float randomSpread = rand.NextFloat(-0.04f, 0.04f) + rand.NextFloat(-0.04f, 0.04f);
                    Vector2 vel = 11f * new Vector2((float)Math.Cos(aimAngle + randomSpread), (float)Math.Sin(aimAngle + randomSpread));
                    EntityManager.Add(new EnemyBullet(Position , vel));
                    GameRoot.Shot.Play(0.2f, rand.NextFloat(-0.2f, 0.2f), 0);
                }
                if (cooldownRemaining > 0)
                    cooldownRemaining--;
            }
            Position += Velocity;
            Position = Vector2.Clamp(Position,new Vector2(Size.X,-50), new Vector2(GameRoot.ScreenSize.X-Size.X,GameRoot.ScreenSize.Y));

            Velocity *= 0.8f;

            if(Position.Y >= GameRoot.ScreenSize.Y)
            {
                IsExpired = true;
            }
        }

        private void AddBehaviour(IEnumerable<int> behaviour)
        {
            behaviours.Add(behaviour.GetEnumerator());
        }

        private void ApplyBehaviours()
        {
            for (int i = 0; i < behaviours.Count; i++)
            {
                if (!behaviours[i].MoveNext())
                    behaviours.RemoveAt(i--);
            }
        }

        public void HandleCollision(Enemy other)
        {
            var d = Position - other.Position;
            Velocity += 15 * d / (d.LengthSquared() + 1);
        }
        #region Take damage or killed
        public void WasShot(int dmg)
        {
            health -= dmg;
            if (health <= 0)
            {
                IsExpired = true;
                KillBullet();
                GameRoot.Explosion.Play(0.5f, rand.NextFloat(-0.2f, 0.2f), 0);
                GameRoot.Score(100);
                SpawnPowerUp();
            }

        }
        public void SpawnPowerUp()
        {
            Vector2 speed = new Vector2(0, 3);
            if (enemyType == "Boss"){
                EntityManager.Add(new BossPowerUp(Position, speed));
            }
            else{
                float Chance = rand.NextFloat(0, 1);//Generates a random float between 0 and 1
                if (Chance <= ChanceOfUpgrade)
                {//if the float is 0.2 or less than a upgrade is dropped, this will add extra bullets and later on add damage that the bullets do
                    EntityManager.Add(new DoubleShot(Position, speed));
                }
                else if (Chance >= ChanceOfExtraLife)
                {//if the float is 0.9 or more than a extra life is dropped
                    EntityManager.Add(new AddionalLives(Position, speed));
                }//This is done to ensure that one enemy doesn't drop 2 power ups
            }
        }
        public void KillBullet()
        {
            for(int j = 0; j < bullets.Count; j++)
            {
                bullets[j].IsExpired = true;
            }
        }
        public void Remove()
        {
            IsExpired = true;
            KillBullet();
            GameRoot.Explosion.Play(0.5f, rand.NextFloat(-0.2f, 0.2f), 0);
        }
        #endregion
        #endregion
        #region Behaviours
        IEnumerable<int> Movemnet(float acceleration = 1f)
        {
            float randomLocation = rand.NextFloat(50, GameRoot.ScreenSize.Y / 3);
            while (true)
            {
                
                if (!PlayerShip.Instance.IsDead)
                    Velocity += new Vector2(0,1);

                if (Velocity != Vector2.Zero)
                    Orientation = Velocity.ToAngle();

                if (enemyType != "Yellow" && Position.Y >= randomLocation)
                {
                    Velocity = Vector2.Zero;

                    Orientation = (PlayerShip.Instance.Position-Position).ToAngle();
                }
                
                

                yield return 0;
            }
        }

        #endregion
    }
}
