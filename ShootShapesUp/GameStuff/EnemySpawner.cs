﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShootShapesUp
{
     class EnemySpawner
    {
        static Random rand = new Random();
        static float inverseSpawnChance = 30;
        static int enemiesSpawned = 0;
        public static void Update()
        {

            if (!PlayerShip.Instance.IsDead && EntityManager.Count < 50 && enemiesSpawned <=GameRoot.enemiesPerRound)
            {
                if (rand.Next((int)inverseSpawnChance) == 0)
                {
                    enemiesSpawned++;
                    EntityManager.Add(Enemy.CreateSeeker(GetSpawnPosition()));
                }
            }

            // slowly increase the spawn rate as time progresses
            if (inverseSpawnChance > 1)
                inverseSpawnChance -= 0.005f;
        }

        private static Vector2 GetSpawnPosition()
        {
            Vector2 pos;
            do
            {
                pos = new Vector2(rand.Next((int)GameRoot.ScreenSize.X-2), -250);
            }
            while (Vector2.DistanceSquared(pos, PlayerShip.Instance.Position) < 250 * 250);

            return pos;
        }

        public static int GetEnemiesSpawned
        {
            get { return enemiesSpawned; }
            set { enemiesSpawned = value; }
        }
        

        public static void Reset()
        {
            inverseSpawnChance = 60;
        }
    }
}
