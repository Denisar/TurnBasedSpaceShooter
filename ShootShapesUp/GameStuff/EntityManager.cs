﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShootShapesUp
{
     static class EntityManager
    {
        static List<Entity> entities = new List<Entity>();
        static List<Enemy> enemies = new List<Enemy>();
        static List<Bullet> bullets = new List<Bullet>();
        static List<EnemyBullet> eBullet = new List<EnemyBullet>();
        static List<DoubleShot> DoubleShot = new List<DoubleShot>();
        static List<AddionalLives> Heart = new List<AddionalLives>();
        static List<BossPowerUp> PowerUp = new List<BossPowerUp>();

        static bool isUpdating;
        static List<Entity> addedEntities = new List<Entity>();

        public static int Count { get { return entities.Count; } }
        public static int GetEnemiesSpawned
        {
            get { return enemies.Count; }private set { }
        }
        public static void Add(Entity entity)
        {
            if (!isUpdating)
                AddEntity(entity);
            else
                addedEntities.Add(entity);
        }

        private static void AddEntity(Entity entity)
        {
            entities.Add(entity);
            if (entity is EnemyBullet)
                eBullet.Add(entity as EnemyBullet);
            else if (entity is Enemy)
                enemies.Add(entity as Enemy); 
            else if (entity is Bullet)
                bullets.Add(entity as Bullet);
            else if (entity is DoubleShot)
                DoubleShot.Add(entity as DoubleShot);
            else if (entity is AddionalLives)
                Heart.Add(entity as AddionalLives);
            else if (entity is BossPowerUp)
                PowerUp.Add(entity as BossPowerUp);

        }

        public static void Update()
        {
            isUpdating = true;
            HandleCollisions();

            foreach (var entity in entities)
                entity.Update();

            isUpdating = false;

            foreach (var entity in addedEntities)
                AddEntity(entity);

            addedEntities.Clear();

            
            
            entities = entities.Where(x => !x.IsExpired).ToList();
            bullets = bullets.Where(x => !x.IsExpired).ToList();
            enemies = enemies.Where(x => !x.IsExpired).ToList();
            eBullet = eBullet.Where(x => !x.IsExpired).ToList();
            DoubleShot = DoubleShot.Where(x => !x.IsExpired).ToList();
            Heart = Heart.Where(x => !x.IsExpired).ToList();
            PowerUp = PowerUp.Where(x => !x.IsExpired).ToList();

            if (GameOverScreen.GetGameStatus == true)
                bullets.Clear();
        }
        public static void BossPowerUp()
        {
            enemies.ForEach(x => x.Remove());
            eBullet.ForEach(x => x.IsExpired = true);
        }
        static void HandleCollisions()
        {
            // handle collisions between enemies
            for (int i = 0; i < enemies.Count; i++)
                for (int j = i + 1; j < enemies.Count; j++)
                {
                    if (IsColliding(enemies[i], enemies[j]))
                    {
                        enemies[i].HandleCollision(enemies[j]);
                        enemies[j].HandleCollision(enemies[i]);
                    }
                }

            // handle collisions between bullets and enemies
            for (int i = 0; i < enemies.Count; i++)
                for (int j = 0; j < bullets.Count; j++)
                {
                    if (IsColliding(enemies[i], bullets[j]))
                    {
                        enemies[i].WasShot(10);
                        bullets[j].IsExpired = true;
                    }
                }

            // handle collisions between the player and enemies
            for (int i = 0; i < enemies.Count; i++)
            {
                if (enemies[i].IsActive && IsColliding(PlayerShip.Instance, enemies[i]))
                {
                    enemies[i].IsExpired = true;
                    PlayerShip.Instance.Kill();
                    checkHp();
                    
                }
            }
            for (int i = 0; i < DoubleShot.Count; i++)
            {
                if ( IsColliding(PlayerShip.Instance,DoubleShot[i]))
                {
                    DoubleShot[i].IsExpired = true;
                    PlayerShip.AddUpgrade();

                }
            }
            for (int i = 0; i < PowerUp.Count; i++)
            {
                if (IsColliding(PlayerShip.Instance, PowerUp[i]))
                {
                    PowerUp[i].IsExpired = true;
                    PlayerShip.PowerUp();

                }
            }
            for (int i = 0; i < Heart.Count; i++)
            {
                if (IsColliding(PlayerShip.Instance, Heart[i]))
                {
                   Heart[i].IsExpired = true;
                    PlayerShip.MoreLife();

                }
            }
            for (int i = 0; i < eBullet.Count; i++)
            {
                if (IsColliding(PlayerShip.Instance, eBullet[i]))
                {
                    eBullet[i].IsExpired = true;
                    PlayerShip.Instance.Kill();
                    checkHp();
                }
            }
        }
        public static void checkHp()
        {
            if (PlayerShip.LivesRemaining() <= 0)
            {
                enemies.ForEach(x => x.Remove());
                eBullet.ForEach(x => x.IsExpired = true);
                Heart.ForEach(x => x.IsExpired = true);
                DoubleShot.ForEach(x => x.IsExpired = true);
                PowerUp.ForEach(x => x.IsExpired = true);
                EnemySpawner.Reset();
            }
        }

        private static bool IsColliding(Entity a, Entity b)
        {
            float radius = a.Radius + b.Radius;
            
            return !a.IsExpired && !b.IsExpired && Vector2.DistanceSquared(a.Position, b.Position) < radius * radius;
        }

        public static void Draw(SpriteBatch spriteBatch)
        {
            foreach (var entity in entities)
                entity.Draw(spriteBatch);
        }
    }
}
