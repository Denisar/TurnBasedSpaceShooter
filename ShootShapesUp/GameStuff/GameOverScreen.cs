﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShootShapesUp
{
     class GameOverScreen
    {
         private static bool isGameOver = false;
        
        public static void Draw(SpriteBatch spriteBatch)
        {

            spriteBatch.Begin();
            spriteBatch.DrawString(GameRoot.Font, "Game Over",new Vector2(GameRoot.ScreenSize.X/3,(GameRoot.ScreenSize.Y/2)-100),Color.White);
            spriteBatch.DrawString(GameRoot.Font, "Score: "+GameRoot.TotalScore.ToString(), new Vector2(GameRoot.ScreenSize.X/3, GameRoot.ScreenSize.Y/2 ), Color.White);
            spriteBatch.DrawString(GameRoot.Font, "Press Space/A to continue", new Vector2(GameRoot.ScreenSize.X / 3, (GameRoot.ScreenSize.Y / 2)+100), Color.White);
            spriteBatch.End();
        }
        

        public static bool GetGameStatus
        {
            get { return isGameOver; }
            set { isGameOver = value; }
        }

    }
}
