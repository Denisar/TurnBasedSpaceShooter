﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShootShapesUp
{
    class PlayerShip : Entity
    {
        #region Getters and Setters
        private static PlayerShip instance;
        public static PlayerShip Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new PlayerShip();
                    
                }
                return instance;
            }
        }
        public static int GetPlayerDamage
        {
           get { return dmg; }
        }
        public static int BossPowerUps
        {
            get { return BossPowerUp; }
        }
        #endregion
        #region Properties
        const int cooldownFrames = 10;
        int cooldownRemaining = 0;

        int framesUntilRespawn = 0;

        int invincibilityTime = 60;
        int invincibilityTimeRemaining = 0;
        static int dmg = 10;

        
        public bool IsDead { get { return framesUntilRespawn > 0; } }
        static int maxLives = 3;
        static int currentLives = 0;
        static Random rand = new Random();
        static int upgrade = 0;
        static int BossPowerUp = 0;
        #endregion
        #region Methods
        private PlayerShip()
        {
            currentLives = maxLives;
            image = GameRoot.Player;
            Position = new Vector2(GameRoot.ScreenSize.X/2,GameRoot.ScreenSize.Y-100 - Size.Y);
            Radius = 10;
        }
        public static int LivesRemaining()
        {
            return currentLives;
        }
        public override void Update()
        {
            if (IsDead && framesUntilRespawn < 30)
                LivesReset();
            if (IsDead )
            {
                --framesUntilRespawn;
                return;
            }

            var aim = Input.GetAimDirection();
            if (aim.LengthSquared() > 0 && cooldownRemaining <= 0)
            {
                cooldownRemaining = cooldownFrames;

                switch (upgrade)
                {
                    case 0: ShottingBullets(0);break;
                    case 1: ShottingBullets(8); ShottingBullets(-8);break;
                    case 2: ShottingBullets(8); ShottingBullets(-8); ShottingBullets(20); ShottingBullets(-20);break;
                }
            }
            
            if((Input.WasButtonPressed(Buttons.A)|| Input.WasKeyPressed(Keys.Q))&& BossPowerUp >= 1)
            {
                EntityManager.BossPowerUp();
                BossPowerUp--;
            }
            

            #region CoolDownResets  
            if (cooldownRemaining > 0)
                cooldownRemaining--;

            
            if (invincibilityTimeRemaining > 0) {
                this.color = Color.Red;
                invincibilityTimeRemaining--;}
            else{this.color = Color.White;}

                #endregion

                const float speed = 8;
            Velocity = speed * Input.GetMovementDirection();
            Position += Velocity;
            Position = Vector2.Clamp(Position, Size / 2, GameRoot.ScreenSize - Size / 2);

        }
        public override void Draw(SpriteBatch spriteBatch)
        {
            if (!IsDead)
                base.Draw(spriteBatch);
        }

        public void LivesReset()
        {
            currentLives = maxLives; 
        }
        public void ShottingBullets(int spread)
        {
            Vector2 vel = 11f * new Vector2(0, -1);

            Vector2 offset = new Vector2(spread, -25);
            EntityManager.Add(new Bullet(Position + offset, vel));
            GameRoot.Shot.Play(0.2f, rand.NextFloat(-0.2f, 0.2f), 0);
        }

        public void Kill()
        {
            if (invincibilityTimeRemaining <= 0)
            {
                invincibilityTimeRemaining = invincibilityTime;
                currentLives--;
                if (currentLives <= 0)
                {
                    GameOverScreen.GetGameStatus = true ;
                    upgrade = 0;
                    framesUntilRespawn = 60;
                    
                }
                
            }
        }
        public static void AddUpgrade()
        {
            if (upgrade < 2)
                upgrade++;
            else
            {
                GameRoot.Score(100);
                dmg += 10;
            }
        }
        public static void PowerUp()
        {
            BossPowerUp++;
        }
        public static void MoreLife()
        {
            if (currentLives < maxLives)
                currentLives++;
            else
                GameRoot.Score(100);
        }

        #endregion 
    }
}
