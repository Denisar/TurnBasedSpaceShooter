﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShootShapesUp
{
    class NewLevelDisplay
    {
        

        static int levelNumber =1;
        public NewLevelDisplay()
        {
            
        }
        public static void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Begin();
            spriteBatch.DrawString(GameRoot.Font, "Level: 0" + levelNumber, new Vector2(GameRoot.ScreenSize.X/4,20), Color.White);
            spriteBatch.End();
        }
        public static void NextLevel()
        {
            levelNumber++;
        }
        public static void Reset()
        {
            levelNumber = 0;
        }
        public static int GetLevelNumber
        {
            get { return levelNumber; }private set { }
        }

    }
}
